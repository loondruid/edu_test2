// requires
let gulp = require('gulp')
let ts = require('gulp-typescript')
let open = require('gulp-open')

// some config
let tsProject = ts.createProject('tsconfig.json')


// tasks
gulp.task("buildTypeScript", function () {
    return tsProject.src()
        .pipe(tsProject())
        .js.pipe(gulp.dest("bin/scripts"));
});       

gulp.task('copyAssets', function () {
    return gulp.src("assets/**")
        .pipe(gulp.dest("bin/assets"));
})

gulp.task("moveOtherFiles", function () {
    return gulp.src([
        "src/**.htm",
        "src/**.html",
        "src/**.jpeg",
        "src/**.png",
        "vendor/**",
    ]).pipe(gulp.dest("bin"));
})

gulp.task('fullBuild', [
    "buildTypeScript",
    "moveOtherFiles",
    "copyAssets"
])

gulp.task('firefox', function () {
    return gulp.src('bin/index.html')
    .pipe(open());
})

gulp.task("default", ['fullBuild'])

gulp.task("br", ['fullBuild', 'firefox'])
