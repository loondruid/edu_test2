/// <reference path="phaser/phaser.d.ts"/>
/// <reference path="config.ts"/>
/// <reference path="MainGameState.ts"/>
window.onload = function () {
    var game = new Phaser.Game(GAME_PIXEL_WIDTH, GAME_PIXEL_HEIGHT, Phaser.AUTO, 'game');
    game.state.add('main', MainGameState);
    game.state.start('main');
};
