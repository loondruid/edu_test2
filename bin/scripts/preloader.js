/// <reference path="candy.ts"/>
var Candy;
(function (Candy) {
    var Preloader = (function () {
        function Preloader(game) {
            this.game = game;
        }
        Preloader.prototype.preload = function () {
            this.game.stage.backgroundColor = '#B4D9E7';
            this.game.preloadBar = this.game.add.sprite((Candy.GAME_WIDTH - 311) / 2, (Candy.GAME_HEIGHT - 27) / 2, 'preloaderBar');
            this.game.load.setPreloadSprite(this.game.preloadBar);
            this.game.load.image('background', 'assets/images/background.png');
            this.game.load.image('floor', 'assets/images/floor.png');
            this.game.load.image('monster-cover', 'assets/images/monster-cover.png');
            this.game.load.image('title', 'assets/images/title.png');
            this.game.load.image('game-over', 'assets/images/gameover.png');
            this.game.load.image('score-bg', 'assets/images/score-bg.png');
            this.game.load.image('button-pause', 'assets/images/button-pause.png');
            this.game.load.spritesheet('candy', 'assets/images/candy.png', 82, 98);
            this.game.load.spritesheet('monster-idle', 'assets/images/monster-idle.png', 103, 131);
            this.game.load.spritesheet('button-start', 'assets/images/button-start.png', 401, 143);
        };
        Preloader.prototype.create = function () {
            this.game.state.start('MainMenu');
        };
        return Preloader;
    }());
    Candy.Preloader = Preloader;
})(Candy || (Candy = {}));
