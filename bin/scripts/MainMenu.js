/// <reference path="candy.ts" />
var Candy;
(function (Candy) {
    var MainMenu = (function () {
        function MainMenu(game) {
            console.log("creating hame " + game);
            this.game = game;
        }
        MainMenu.prototype.create = function () {
            this.game.add.sprite(0, 0, 'background');
            this.game.add.sprite(-130, Candy.GAME_HEIGHT - 514, 'monster-cover');
            this.game.add.sprite((Candy.GAME_WIDTH - 395) / 2, 60, 'title');
            var self = this;
            this.game.add.button(Candy.GAME_WIDTH - 401 - 10, Candy.GAME_HEIGHT - 143 - 10, 'button-start', function () { self.startGame(); }, this.game, 1, 0, 2);
        };
        MainMenu.prototype.startGame = function () {
            console.log(this.game);
            this.game.state.start('Game');
        };
        return MainMenu;
    }());
    Candy.MainMenu = MainMenu;
})(Candy || (Candy = {}));
