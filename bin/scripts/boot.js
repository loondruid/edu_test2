var Candy;
(function (Candy) {
    var Boot = (function () {
        function Boot(game) {
            this.game = game;
        }
        Boot.prototype.preload = function () {
            this.game.load.image('preloaderBar', 'assets/images/loading-bar.png');
        };
        Boot.prototype.create = function () {
            this.game.input.maxPointers = 1;
            this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.game.scale.pageAlignHorizontally = true;
            this.game.scale.pageAlignVertically = true;
            this.game.scale.updateLayout();
            this.game.state.start('Preloader');
        };
        return Boot;
    }());
    Candy.Boot = Boot;
})(Candy || (Candy = {}));
