var Candy;
(function (Candy) {
    var scoreText = null;
    var score = 0;
    var health = 10;
    Candy.GAME_HEIGHT = 960;
    Candy.GAME_WIDTH = 640;
    var Game = (function () {
        function Game(game) {
            this.game = game;
            this.player = null;
            this.candyGroup = [];
            this.spawnCandyTimer = 0;
            this.fontStyle = null;
        }
        Game.prototype.create = function () {
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.physics.arcade.gravity.y = 200;
            this.game.add.sprite(0, 0, 'background');
            this.game.add.sprite(-30, Candy.GAME_HEIGHT - 160, 'floor');
            this.game.add.sprite(10, 5, 'score-bg');
            var self = this;
            this.game.add.button(Candy.GAME_WIDTH - 96 - 10, 5, 'button-pause', function () { self.managePause(); });
            this.player = this.game.add.sprite(5, 760, 'monster-idle');
            this.player.animations.add('idle', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], 10, true);
            this.player.animations.play('idle');
            this.spawnCandyTimer = 0;
            health = 10;
            this.fontStyle = { font: "40px Arial", fill: "#FFCC00", stroke: "#333", strokeThickness: 5, align: "center" };
            scoreText = this.game.add.text(120, 20, "0", this.fontStyle);
            this.candyGroup = this.game.add.group();
            item.spawnCandy(this);
        };
        Game.prototype.managePause = function () {
            this.game.paused = true;
            var pausedText = this.game.add.text(100, 250, "Game paused.\nTap anywhere to continue.", this.fontStyle);
            this.game.input.onDown.add(function () {
                pausedText.destroy();
                this.paused = false;
            }, this.game);
        };
        Game.prototype.update = function () {
            this.spawnCandyTimer += this.game.time.elapsed;
            if (this.spawnCandyTimer > 1000) {
                this.spawnCandyTimer = 0;
                item.spawnCandy(this);
            }
            this.candyGroup.forEach(function (candy) {
                candy.angle += candy.rotateMe;
            });
            if (health <= 0) {
                this.game.add.sprite((Candy.GAME_WIDTH - 594) / 2, (Candy.GAME_HEIGHT - 271) / 2, 'game-over');
                this.game.paused = true;
            }
        };
        return Game;
    }());
    Candy.Game = Game;
    var item = (function () {
        function item() {
        }
        item.spawnCandy = function (game) {
            var dropPos = Math.floor(Math.random() * Candy.GAME_WIDTH);
            var dropOffset = [100, 100, 100, 100, 100];
            var candyType = Math.floor(Math.random() * 5);
            var candy = game.add.sprite(dropPos, dropOffset[candyType], 'candy');
            candy.animations.add('anim', [candyType], 10, true);
            candy.animations.play('anim');
            game.physics.enable(candy, Phaser.Physics.ARCADE);
            candy.inputEnabled = true;
            candy.game = game;
            candy.events.onInputDown.add(this.clickCandy, this);
            candy.checkWorldBounds = true;
            candy.events.onOutOfBounds.add(this.removeCandy, this);
            candy.anchor.setTo(0.5, 0.5);
            candy.rotateMe = (Math.random() * 4) - 2;
            game.candyGroup.add(candy);
        };
        item.clickCandy = function (candy) {
            candy.kill();
            score += 1;
            scoreText.setText(score);
            //item.spawnCandy(candy.game);
        };
        item.removeCandy = function (candy) {
            candy.kill();
            health -= 10;
        };
        return item;
    }());
    Candy.item = item;
})(Candy || (Candy = {}));
