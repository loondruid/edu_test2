/// <reference path="phaser/phaser.d.ts"/>
/// <reference path="config.ts"/>
var MainGameState = (function () {
    function MainGameState(game) {
        this.game = game;
    }
    //preload everything needed for this state
    MainGameState.prototype.preload = function () {
        this.game.load.image('sky', 'assets/images/sky.png');
        this.game.load.image('ground', 'assets/images/platform.png');
        this.game.load.image('star', 'assets/images/star.png');
        this.game.load.spritesheet('dude', 'assets/images/dude.png', 32, 48);
    };
    //create - do something with preloaded things once preload done
    MainGameState.prototype.create = function () {
        this.game.input.maxPointers = 1;
        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.game.scale.pageAlignHorizontally = true;
        this.game.scale.pageAlignVertically = true;
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.add.sprite(0, 0, 'sky');
        this.platforms = this.game.add.group();
        this.platforms.enableBody = true;
        var ground = this.platforms.create(0, this.game.world.height - 64, 'ground');
        ground.scale.setTo(2, 2);
        ground.body.immovable = true;
        var ledge = this.platforms.create(400, 400, 'ground');
        ledge.body.immovable = true;
        ledge = this.platforms.create(-150, 250, 'ground');
        ledge.body.immovable = true;
        this.player = this.game.add.sprite(32, this.game.world.height - 150, 'dude');
        this.game.physics.enable(this.player);
        this.player.body.bounce.y = 0.2;
        this.player.body.gravity.y = 300;
        this.player.body.collideWorldBounds = true;
        this.player.animations.add('left', [0, 1, 2, 3], 10, true);
        this.player.animations.add('right', [5, 6, 7, 8], 10, true);
        this.stars = this.game.add.group();
        this.stars.enableBody = true;
        for (var i = 0; i < 6; i++)
            this.starburn();
        this.score = 0;
        this.scoreText = this.game.add.text(16, 16, 'Score: 0', { fontSize: '32px', fill: '#000' });
    };
    MainGameState.prototype.starburn = function () {
        var star = this.stars.create(Math.random() * 12 * 70, 0, 'star');
        star.body.gravity.y = 6;
        star.body.bounce.y = 0.7 + Math.random() * 0.2;
    };
    //update - runs on each frame draw
    MainGameState.prototype.update = function () {
        var hitPlatform = this.game.physics.arcade.collide(this.player, this.platforms);
        var cursors = this.game.input.keyboard.createCursorKeys();
        if (cursors.left.isDown) {
            this.player.body.velocity.x = -150;
            this.player.animations.play('left');
        }
        else if (cursors.right.isDown) {
            this.player.body.velocity.x = 150;
            this.player.animations.play('right');
        }
        else {
            this.player.animations.stop();
            this.player.frame = 4;
        }
        if (cursors.up.isDown && this.player.body.touching.down && hitPlatform) {
            this.player.body.velocity.y = -350;
        }
        this.game.physics.arcade.collide(this.stars, this.platforms);
        this.game.physics.arcade.overlap(this.player, this.stars, this.collectStar, null, this);
    };
    MainGameState.prototype.collectStar = function (player, star) {
        star.kill();
        this.score += 1529;
        this.scoreText.text = "Score: " + this.score;
    };
    return MainGameState;
}());
